package com.example.yotubeadv;

import java.util.List;




import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListAdapter;
import android.widget.ListView;

public class VideosListView extends ListView{

	public VideosListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}
	public VideosListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public VideosListView(Context context) {
		super(context);
	}
	
	public void setVideos(List<Video> videos){
		VideosAdapter adapter = new VideosAdapter(getContext(), videos);
		setAdapter(adapter);
	}
	@Override
	public void setAdapter(ListAdapter adapter) {
		super.setAdapter(adapter);
	}

}
