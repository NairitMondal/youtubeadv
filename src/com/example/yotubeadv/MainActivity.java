package com.example.yotubeadv;





import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;

public class MainActivity extends Activity {
	
	 // A reference to our list that will hold the video details
		private VideosListView listView;
		
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		listView = (VideosListView) findViewById(R.id.videosListView);
	}
	
	// This is the XML onClick listener to retreive a users video feed
	 public void getUserYouTubeFeed(View v){
		// We start a new task that does its work on its own thread
	    	// We pass in a handler that will be called when the task has finished
	    	// We also pass in the name of the user we are searching YouTube for
	    	new GetYouTubeUserVideosTask(responseHandler, "CarR_girNJ1BX7RiChQxuQ").run();
	    }
	   
	 // This is the handler that receives the response when the YouTube task has finished
		@SuppressLint("HandlerLeak")
		Handler responseHandler = new Handler() {
			public void handleMessage(Message msg) {
				populateListWithVideos(msg);
			};
		};
		/**
		 * This method retrieves the Library of videos from the task and passes them to our ListView
		 * @param msg
		 */
		private void populateListWithVideos(Message msg) {
			// Retreive the videos are task found from the data bundle sent back
			Library lib = (Library) msg.getData().get(GetYouTubeUserVideosTask.LIBRARY);
			// Because we have created a custom ListView we don't have to worry about setting the adapter in the activity
			// we can just call our custom method with the list of items we want to display
			listView.setVideos(lib.getVideos());
		}
		@Override
		protected void onStop() {
			// Make sure we null our handler when the activity has stopped
			// because who cares if we get a callback once the activity has stopped? not me!
			responseHandler = null;
			super.onStop();
		
	
	
	
	
	
	

	
		
	}
}
