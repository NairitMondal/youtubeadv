package com.example.yotubeadv;

import java.io.Serializable;

public class Video implements Serializable {
	
	private String title;

	// A link to the video on youtube
	private String url;
	// A link to a still image of the youtube video
	
	public Video(String title, String url) {
		super();
		this.title = title;
		this.url = url;
		
	}
	public String getUrl() {
		return url;
	}
	
	
	
	public CharSequence getTitle() {
		// TODO Auto-generated method stub
		return title;
	}

	

}
