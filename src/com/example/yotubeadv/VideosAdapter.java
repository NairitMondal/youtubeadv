package com.example.yotubeadv;

import java.util.List;




import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class VideosAdapter extends BaseAdapter {
	List<Video> videos;
	private LayoutInflater mInflater;

	public VideosAdapter(Context context, List<Video> videos) {
		this.videos = videos;
		this.mInflater = LayoutInflater.from(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return videos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return videos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		// If convertView wasn't null it means we have already set it to our list_item_user_video so no need to do it again
				if(convertView == null){
					// This is the layout we are using for each row in our list
					// anything you declare in this layout can then be referenced below
					convertView = mInflater.inflate(R.layout.listitem,null);
				}
				// We are using a custom imageview so that we can load images using urls
				// For further explanation see: http://blog.blundell-apps.com/imageview-with-loading-spinner/
			
				
				TextView title = (TextView) convertView.findViewById(R.id.userVideoTitleTextView); 
				// Get a single video from our list
				Video video = videos.get(position);
				// Set the image for the list item
			
				// Set the title for the list item
				title.setText(video.getTitle());
				
				return convertView;
			}

}
