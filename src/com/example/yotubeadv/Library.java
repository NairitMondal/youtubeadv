package com.example.yotubeadv;

import java.io.Serializable;
import java.util.List;



public class Library implements Serializable {
	// The username of the owner of the library
	 
	final String	user ;
		// A list of videos that the user owns
		private List<Video> videos;
		public Library(String user, List<Video> videos) {
			this.user=user;
			this.videos = videos;
		}
		
		
		 
			public String getUser() {
				return user;
			}

			 
				public List<Video> getVideos() {
					return videos;
				}

}
